from django.urls import path
from todos.views import (
    ToDoDeleteView,
    TodoListView,
    ToDoDetailView,
    ToDoUpdateView,
    ToDoCreateView,
    TodoItemCreateView,
    ToDoItemUpdateView,
)


urlpatterns = [
    path("", TodoListView.as_view(), name="todos_list"),
    path("<int:pk>/", ToDoDetailView.as_view(), name="show_todolist"),
    path("<int:pk>/edit", ToDoUpdateView.as_view(), name="update_todolist"),
    path("create/", ToDoCreateView.as_view(), name="create_todolist"),
    path("<int:pk>/delete", ToDoDeleteView.as_view(), name="delete_todolist"),
    path(
        "items/create",
        TodoItemCreateView.as_view(),
        name="create_todoitem",
    ),
    path(
        "items/<int:pk>/edit",
        ToDoItemUpdateView.as_view(),
        name="update_todoitem",
    ),
]
