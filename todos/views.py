# from django.shortcuts import render
from audioop import reverse
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from todos.models import TodoList, TodoItem
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todo_lists"


class ToDoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

    context_object_name = "todos_list"

    # def get_context_data(self, **kwargs):
    #     context= super(ToDoList, self).get_context_data(**kwargs)

    #     for list in self.


class ToDoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    context_object_name = "newlist"

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/createItem.html"
    fields = ["task", "due_date", "is_completed", "list"]
    context_object_name = "item"

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class ToDoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    context_object_name = "edit_item"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class ToDoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/editItem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class ToDoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    context_object_name = "deleteList"

    success_url = reverse_lazy("todos_list")
